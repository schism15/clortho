import os
import socketserver

class MyTCPHandler(socketserver.BaseRequestHandler):
    '''
    The request handler class for our server.

    It is instantiated once per connection to the server, and must
    override the handle() method to implement communication to the
    client.

    There is no virtual host support in this sever
    '''

    # def _find_gopher_doc_dir(hostname):
    #     # TODO: break this out as a config variable
    #     path = '/home/schism/projects/gozer/test/fixtures/gopher_docs'
    #     site_dirs = [(f.name, f.path) for f in os.scandir(path) if f.is_dir()]

    #     for site_dir in site_dirs:
    #         if hostname == site_dir[0]:
    #             return site_dir[1]
    #         else:
    #             return None

    def handle(self):
        # self.request is the TCP socket connected to the client
        self.data = self.request.recv(1024).strip()
        docroot_dir = '/home/schism/projects/gozer/test/fixtures/docroot/'
        
        if docroot_dir:
            if self.data == b'':
                index_path = os.path.join(docroot_dir, 'index.txt')
                with open(index_path, mode='rb') as f:
                    resp = f.read()
            else:
                # IMP: Add logic for handling selectors
                pass
        else:
            resp = b'Site not found'

        self.request.sendall(resp)

class MockGopherServer():
    def __init__(self):
        self._host = 'localhost'
        self._port = 5000

        with socketserver.TCPServer((self._host, self._port), MyTCPHandler) as server:
            server.serve_forever()
